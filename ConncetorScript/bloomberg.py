import logging
import json
import os
import requests
from pathlib import Path
import pandas as pd

logger = logging.Logger(__name__)
headerss = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
}


class BloombergConnector(object):
    def __init__(self, folder=f"{os.getcwd()}/data"):
        self.base_link = "https://www.bloomberg.com/markets2/api/history/"
        self.folder = folder
        self.ensure_folder(self.folder)

    @staticmethod
    def _get_params(start_date, end_date,
                    currency_symbol='USD', data_frequency='DAILY'):
        return {
            "currency_symbol": currency_symbol,
            "data_frequency": data_frequency,
            "start_date": start_date.strftime("%Y%m%d"),
            "end_date": end_date.strftime("%Y%m%d")

        }

    def download(self, market: list):
        for market_ID in market:
            try:
                logger.info(f"Downloading and processing data for "
                            f"{market_ID.get('ticker')}")
                link1 = f"{self.base_link}{market_ID.get('ticker')}:IND/PX_LAST?timeframe=5_YEAR&period=daily&volumePeriod=daily"
                data = requests.get(
                    link1, headers=headerss, params=self._get_params(
                        start_date=market_ID.get("start_date"),
                        end_date=market_ID.get("end_date"),
                        currency_symbol=market_ID.get("currency_symbol"),
                        data_frequency=market_ID.get("data_frequency")
                    )).json()

                for idx in data:
                    price = idx["price"]
                df = pd.DataFrame.from_dict(price)
                df.rename(columns={'dateTime': 'formattedEffectiveDate',
                                   'value': "indexValue"}, inplace=True)
                df['formattedEffectiveDate'] = pd.to_datetime(
                    df['formattedEffectiveDate'],
                    format='%Y-%m-%d')
                df.set_index("formattedEffectiveDate", inplace=True)
                self.to_csv(f"{market_ID.get('ticker')}_Data", df)

            except Exception as e:
                logger.error(f"Encountered an issue while getting the data from"
                             f"Bloomberg for  "f"{market_ID.get('ticker')}")
                logger.error(e)
                logger.info("Continuing the data processing")

    @staticmethod
    def ensure_folder(folder_path):
        """
        Function to create a folder if path doesn't exist
        """
        Path(folder_path).mkdir(parents=True, exist_ok=True)

    def to_csv(self, name, data):

        data.to_csv(
            f'{os.path.join(self.folder, name.replace("/", " "))}.csv',
            date_format="%d-%b-%Y"
        )


if __name__ == "__main__":
    stream_handler = logging.StreamHandler()
    logger.addHandler(stream_handler)

    end_date = pd.Timestamp.now(tz='US/Eastern')
    start_date = end_date - pd.Timedelta(days=1825)
    data_frequency = 'DAILY'

    Bloomberg_Markets = [
        {
            "ticker": "BACM0",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        },
        {
            "ticker": "BAGV0",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date

        },
        {
            "ticker": "BACR0",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        },
        {
            "ticker": "BAFRN0",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        },
        {
            "ticker": "LEGATRUU",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        },
        {
            "ticker": "BTSYTRUU",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        },
        {
            "ticker": "LGCPTREH",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        },
        {
            "ticker": "LG30TRUH",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        },
        {
            "ticker": "CCMP",
            "currency_symbol": "USD",
            "data_frequency": data_frequency,
            "start_date": start_date,
            "end_date": end_date
        }
    ]
    Bloomberg_Connector = BloombergConnector(folder=f"{os.getcwd()}/data")
    logger.info("Processing Market Data from Bloomberg")
    Bloomberg_Connector.download(Bloomberg_Markets)



